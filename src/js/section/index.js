import Hero from './hero';
import Mission from './mission';
import Getting from './getting';
import Service from './services';
import Founders from './founders';
import Connection from './connection';
import Footer from './footer';

export {
  Hero, Mission, Getting, Service, Founders, Connection, Footer
};
