import React from 'react';
import { Modal, ModalBody, ModalHeader } from 'reactstrap';
import Button from '../components/button';

class ContactModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      message: ''
    };
  }

  handleChange(name, event) {
    this.setState({
      [name]: event.target.value
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const {
      state,
      props
    } = this;

    props.onSubmit({
      name: state.name,
      email: state.email,
      message: state.message
    });
  }

  render() {
    const {
      state,
      props
    } = this;
    return (
      <Modal isOpen={props.isOpen} toggle={props.onToggle} className="modal-dialog-contact" centered>
        <ModalHeader toggle={props.onToggle}>
          <span className="h3 h1-md">Let’s get connected.</span>
          <div className="font-size-base font-weight-normal">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus tempus rutrum porta. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </div>
        </ModalHeader>
        <ModalBody>
          <form onSubmit={this.handleSubmit.bind(this)} action="">
            <div className="form-group">
              <label htmlFor="name">Your name</label>
              <input
                type="name"
                name="name"
                id="name"
                className="form-control"
                value={state.name}
                required
                onChange={this.handleChange.bind(this, 'name')} />
            </div>
            <div className="form-group">
              <label htmlFor="email">Your email</label>
              <input
                type="email"
                name="email"
                id="email"
                className="form-control"
                value={state.email}
                required
                onChange={this.handleChange.bind(this, 'email')} />
            </div>
            <div className="form-group">
              <label htmlFor="message">What do you want to tell us?</label>
              <textarea
                name="message"
                id="message"
                className="form-control"
                value={state.message}
                required
                rows="6"
                onChange={this.handleChange.bind(this, 'message')} />
            </div>
            <div>
              <button type="submit" className="btn btn-light">
                <div className="btn-outer">
                  <div className="btn-inner">
                    Send →
                  </div>
                </div>
              </button>
            </div>
          </form>
        </ModalBody>
      </Modal>
    );
  }
}

ContactModal.defaultProps = {
  isOpen: true,
  toggle: () => {},
  onSubmit: () => {},
  className: 'modal-contact'
};

export default ContactModal;
