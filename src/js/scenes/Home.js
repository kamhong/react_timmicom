import React from 'react';
import {
  Hero, Mission, Getting, Service, Founders, Connection, Footer
} from '../section';
import ContactModal from '../section/contact-modal';

export class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isContactModalOpen: false
    };
  }

  handleContactSubmit(values) {
    console.log(values);
  }

  handleContactModalToggle() {
    this.setState({
      isContactModalOpen: !(this.state.isContactModalOpen)
    });
  }

  render() {
    const {
      state
    } = this;

    return (
      <div>
        <Hero onContactClick={this.handleContactModalToggle.bind(this)} />
        <Mission />
        <Getting />
        <Service />
        <Founders />
        <Connection onContactClick={this.handleContactModalToggle.bind(this)} />
        <ContactModal
          isOpen={state.isContactModalOpen}
          onToggle={this.handleContactModalToggle.bind(this)}
          onSubmit={this.handleContactSubmit.bind(this)}
        />
        <Footer />
      </div>
    );
  }
}
